# Input
# input() allows us to gather data from the user input, returns 
# "string" data type
#  "\n" stands for line break

# username = input("Please enter your name: \n")

# print(f"Hi {username}! Welcome to Python Short Course.")

# num1 = input("Enter 1st number: \n")
# num2 = input("Enter 2nd number: \n")
# print(f"The sum of num1 and num2 is {num1 + num2}")


# num1 = int(input("Enter 1st number: \n"))
# num2 = int(input("Enter 2nd number: \n"))

# print(f"The sum of num1 and num2 is {num1 + num2}")



# If-else statements
# If-else statements are used to choose between two or more
# code blocks depending on the condition

# Declare a variable to use for the conditional statement

# test_num = 75

# if  test_num >= 60:
# 	print("Test Passed.")

# else:
# 	print("Test failed.")	

# Else-if chains
# test_num2 = int(input("Please enter the 2nd test number \n"))

# if test_num2 > 0:
# 	print("The number is positive")

# elif test_num2 == 0:
# 	print("The number is zero")
# else:
# 	print("The number is negative")

# Mini-Exercise: 2:35PM
# Create an if-else statement that determines if a number is
# divisible by 3, 5 or both.
# If the number is divisible by 3, print "The number is
# divisible by 3"
# If the number is divisible by 5, print "The number is 
# divisible by 5"
# If the number is divisible by 5 and 3, print "The number
# is divisible by both 3 and 5"
# If the number is not divisible by any, print "The number
# is not divisible by 3 nor 5"

# Kindly send the output of your terminal in our group chat.



# test_number = int(input("Enter the number: \n"))


# if test_number % 3 == 0:
# 	if test_number % 5 == 0:
# 		print(f"{test_number} The number is divisible by both 3 and 5")

# 	else:
# 		print("The number is divisible by 3")


# elif test_number % 5 == 0:
# 	print("The number is divisible by 5")

# else:
# 	print("The number is not divisible by 3 nor 5")

# MS MIA CODE

# test_div_num = int(input("Please enter a number: \n"))

# if test_div_num % 3 == 0 and test_div_num % 5 == 0:
# 	print(f"{test_div_num} is divisible by 3 and 5")
# elif test_div_num % 3 == 0:
# 	print(f"{test_div_num} is divisible by 3")
# elif test_div_num % 5 == 0:
# 	print(f"{test_div_num} is divisible by 5")
# else:
# 	print(f"{test_div_num} is not divisible by 3 nor 5")




# Loops
# Python has loops that can repeat blocks of code
# While loops are used to execute a set of statement
# as long as the condition is true


# i = 1
# while i <= 5:
# 	print(f"Current Count {i}")
# 	i += 1


# # For Loops are used for iterating over a sequence
# fruits = ["apol", "saging", "cherryred"]
# for indiv_fruit in fruits:
# 	print(indiv_fruit)


# range() method
# To use the for loop to iterate through values, the 
# range method can be used
# Syntax:
# range(stop)
# range(start, stop)
# range(start, stop, step)


# # The range() - defaults to 0 as a starting value. 0 - 5
# for x in range(6):
# 	print(f"The current value is {x}")


# for x in range(6, 10):
# 	print(f"The current value is {x}")

# for x in range(6, 10, 2):
# 	print(f"The current value is {x}")



# Break Statements
# The break statement is used to stop the loop
# j = 1
# while j < 6:
# 	print(j)
# 	if j == 3:
# 		break
# 	j += 1	

# When we say break, completely terminated

# Continue Statement
# Continute statement it returns the control to the beginning
# of the while loop and continute with tnext iteration

k = 1
while k < 6:
	k += 1
	# k value became 2
	if k == 3:
		continue
	print(k)
	

# Make sure to push activity in gitlab
# Activity should be submitted in google forms
# https://docs.google.com/forms/d/e/1FAIpQLSekS1iIUrW7fyOjR-RhDBYWObpXEXeq43GE4zIhBzi1O9w52w/viewform
































































